package com.example.vacuumlabstest

import android.app.Application
import com.example.vacuumlabstest.di.repositoryModule
import com.example.vacuumlabstest.di.viewModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@App)
            koin.loadModules(modules)
        }
    }

    private val modules = listOf(
        repositoryModule,
        viewModule
    )
}