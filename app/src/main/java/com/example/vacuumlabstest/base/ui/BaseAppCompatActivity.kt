package com.example.vacuumlabstest.base.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.repeatOnLifecycle
import androidx.viewbinding.ViewBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

abstract class BaseAppCompatActivity<ViewBind : ViewBinding, VH : ViewModel> : AppCompatActivity(){

    protected abstract val binding: ViewBind
    protected abstract val viewModel: VH
    private val coroutineScope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        init()
    }

    abstract fun init()

    protected fun <T>launchStatFlow(stateFlow : Flow<T>, action: suspend (T) -> Unit){

        coroutineScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED){
                stateFlow.collectLatest(action)
            }
        }
    }
}