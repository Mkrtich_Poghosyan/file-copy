package com.example.vacuumlabstest.base.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

abstract class BaseViewModel(context : Application) : AndroidViewModel(context) {

    @SuppressLint("StaticFieldLeak")
    val applicationContext: Context = context.applicationContext

    private val mLoading = MutableStateFlow<Boolean>(false)
    var loading = mLoading.asStateFlow()



    fun changeLoad(isLoaded : Boolean){
        changeFlow {
            mLoading.emit(isLoaded)
        }
    }

    protected fun changeFlow(function: suspend () -> Unit) = viewModelScope.launch {
        function.invoke()
    }

    fun  lunch(function: suspend  () -> Unit) = viewModelScope.run {
        viewModelScope.launch(Dispatchers.IO) {
            function.invoke()
        }
    }
}