package com.example.vacuumlabstest.data.repository

import android.content.Context
import android.os.Environment
import android.util.Log
import com.example.vacuumlabstest.R
import com.example.vacuumlabstest.data.datastore.FileCopyRepository
import com.example.vacuumlabstest.entity.CopyException
import com.example.vacuumlabstest.entity.Result
import com.example.vacuumlabstest.entity.enums.ProcessState
import com.example.vacuumlabstest.enums.Storage
import com.example.vacuumlabstest.extensions.generate
import com.example.vacuumlabstest.extensions.save
import com.example.vacuumlabstest.utils.FILE_ALREADY_EXISTS
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.nio.ByteBuffer


class FileCopyRepositoryImpl(private val context: Context) : FileCopyRepository {

    companion object{
        private const val GENERATED_FILE_NAME = "generatedFile"
    }

    override suspend fun copy(
        targetFileName: String,
        chooseFilePath: String,
        storage: Storage
    )  : Result<ProcessState> {

        val source = File(chooseFilePath)
       return copy(
            targetFileName = targetFileName,
            storage = storage,
            source = source
        )

    }

    override suspend fun copy(targetFileName: String, storage: Storage) : Result<ProcessState>{
        val source = context.generate(GENERATED_FILE_NAME, storage)
       return copy(
            targetFileName = targetFileName,
            storage = storage,
            source = source
        )
    }

    private suspend fun copy(targetFileName: String, storage: Storage, source: File)  : Result<ProcessState> {
        val targetFile = when (storage) {
            Storage.INTERNAL -> File(context.filesDir, "$targetFileName.txt")
            Storage.EXTERNAL -> {
                File(Environment.getExternalStorageDirectory(), "$targetFileName.txt")
            }
        }
        if (!source.exists()){
            generateLargeFile(storage)
        }
      return  copyUsingChannel(source,targetFile)
    }

    private fun copyUsingChannel(source: File, target: File) : Result<ProcessState> {

        if (target.exists()){
            return Result.Error(CopyException(null, ProcessState.GENERATE, FILE_ALREADY_EXISTS))
        }

        try {
            source.createNewFile()

            FileInputStream(source).channel.use { inputChannel ->
                FileOutputStream(target).channel.use { outputChannel ->
                    val buffer: ByteBuffer = ByteBuffer.allocateDirect(4 * 1024)
                    while (inputChannel.read(buffer) != -1) {
                        buffer.flip()
                        outputChannel.write(buffer)
                        buffer.clear()
                    }
                }
            }

            return Result.Success(ProcessState.COPY)
        }catch (ioEx : IOException){
            ioEx.printStackTrace()
            return Result.Error(CopyException(ioEx, ProcessState.COPY))
        }
    }

    override suspend fun generateLargeFile(storage : Storage) : Result<ProcessState> {

        val file = when (storage) {
            Storage.INTERNAL -> File(context.filesDir, "$GENERATED_FILE_NAME.txt")
            Storage.EXTERNAL -> {
                File(Environment.getExternalStorageDirectory(), "$GENERATED_FILE_NAME.txt")
            }
        }
        Log.wtf("TESTING", "FileCopyRepositoryImpl generateLargeFile file.path -> ${file.path}")
        Log.wtf("TESTING", "FileCopyRepositoryImpl generateLargeFile file.exists -> ${file.exists()}")

        if (file.exists()){
           return Result.Error(CopyException(null, ProcessState.GENERATE, FILE_ALREADY_EXISTS))
        }
        val text = context.getString(R.string.large_text)
        return  try {
            for (i in 0..20000) {
                file.save(text)
            }
          Result.Success(ProcessState.GENERATE)
        }catch (ioEx : IOException){
          ioEx.printStackTrace()
          return Result.Error(CopyException(ioEx, ProcessState.GENERATE))
        }

    }

}