package com.example.vacuumlabstest.data.datastore

import com.example.vacuumlabstest.entity.Result
import com.example.vacuumlabstest.entity.enums.ProcessState
import com.example.vacuumlabstest.enums.Storage

interface FileCopyRepository {

   suspend fun  copy(targetFileName : String, chooseFilePath: String, storage : Storage) : Result<ProcessState>
   suspend fun  copy(targetFileName : String, storage : Storage) : Result<ProcessState>

   suspend fun generateLargeFile(storage : Storage) : Result<ProcessState>

}