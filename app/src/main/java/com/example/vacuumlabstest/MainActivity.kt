package com.example.vacuumlabstest

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.view.View
import com.example.vacuumlabstest.base.ui.BaseAppCompatActivity
import com.example.vacuumlabstest.binding.viewBinding
import com.example.vacuumlabstest.databinding.ActivityMainBinding
import com.example.vacuumlabstest.entity.Result
import com.example.vacuumlabstest.entity.enums.ProcessState
import com.example.vacuumlabstest.enums.LoadDirection
import com.example.vacuumlabstest.enums.Storage
import com.example.vacuumlabstest.extensions.*
import com.example.vacuumlabstest.utils.WRITE_EXTERNAL_STORAGE_REQUEST_CODE
import com.example.vacuumlabstest.viewmodel.CopyFileViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : BaseAppCompatActivity<ActivityMainBinding, CopyFileViewModel>() {

    override val binding: ActivityMainBinding by viewBinding()
    override val viewModel: CopyFileViewModel by viewModel()
    private val launcher = startActivityForResultEx {
        if (it.resultCode == RESULT_OK) {
            viewModel.setChoseFile(it.data?.data)
        }
    }

    override fun init() {
        binding.copy.setOnClickListener(getOnClickListener(ProcessState.COPY))

        binding.newFileName.textChange {isEnabled, text ->
            binding.copy.isEnabled = isEnabled
            viewModel.textFileName = text
        }

        binding.chooseFile.setOnClickListener {
            launcherStart()
        }

        binding.generate.setOnClickListener(getOnClickListener(ProcessState.GENERATE))

        binding.radioGroupLoad.setOnCheckedChangeListener { _, checkedId ->

            when (checkedId) {
                R.id.storage -> {
                    binding.chooseFile.isEnabled = true
                    binding.generate.isEnabled = false
                    viewModel.loadTo = LoadDirection.STORAGE
                }

                R.id.autoGenerate -> {
                    binding.chooseFile.isEnabled = false
                    binding.generate.isEnabled = true
                    viewModel.loadTo = LoadDirection.GENERATE
                }
            }
        }

        binding.radioGroupSave.setOnCheckedChangeListener{ _, checkedId ->
            when (checkedId) {
                R.id.external -> viewModel.saveTo = Storage.EXTERNAL

                R.id.internal -> viewModel.saveTo = Storage.INTERNAL
            }
        }

        launchStatFlow(viewModel.loading) {
            binding.progressBar.showOrHide(it)
        }

        launchStatFlow(viewModel.result) { result ->
            when (result) {
                is Result.Success -> {
                    binding.root.showSnackBar(result.t, true)
                }

                is Result.Error -> {
                    binding.root.showSnackBar(result.copyException.t, false, result.copyException.errorCode)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty()
            && grantResults[0] == PackageManager.PERMISSION_GRANTED
        ) {
            if (requestCode == WRITE_EXTERNAL_STORAGE_REQUEST_CODE) {
                viewModel.onClick()
            }
        }else{
            viewModel.processState = ProcessState.NONE
        }
    }


    private fun launcherStart(){
        launcher.launch(Intent().apply {
            type = "*/*"
            action = Intent.ACTION_GET_CONTENT

        })
    }

    private fun askPermission(){
        if (checkPermissionGranted(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                WRITE_EXTERNAL_STORAGE_REQUEST_CODE
            )
        ) {

            viewModel.onClick()
        }
    }

    private fun getOnClickListener(processState: ProcessState) : View.OnClickListener{
        return  View.OnClickListener {
            viewModel.processState = processState
            when (binding.radioGroupSave.checkedRadioButtonId) {
                R.id.internal -> {
                    viewModel.onClick()
                }

                R.id.external -> {
                    askPermission()
                }
            }
        }
    }

}