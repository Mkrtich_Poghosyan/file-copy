package com.example.vacuumlabstest.viewmodel

import android.app.Application
import android.net.Uri
import com.example.vacuumlabstest.base.viewmodel.BaseViewModel
import com.example.vacuumlabstest.data.datastore.FileCopyRepository
import com.example.vacuumlabstest.entity.Result
import com.example.vacuumlabstest.entity.enums.ProcessState
import com.example.vacuumlabstest.enums.LoadDirection
import com.example.vacuumlabstest.enums.Storage
import com.example.vacuumlabstest.utils.URIPathHelper
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow

class CopyFileViewModel(private val fileRepository: FileCopyRepository, app: Application) :
    BaseViewModel(app) {

    private var chooseFileUri: Uri? = null

    private val mResult = MutableSharedFlow<Result<ProcessState>>()
    var result = mResult.asSharedFlow()

    var saveTo : Storage = Storage.INTERNAL
    var loadTo : LoadDirection = LoadDirection.GENERATE
    var textFileName : String = ""
    var processState : ProcessState = ProcessState.NONE

    fun onClick(){

        if (processState == ProcessState.GENERATE){
            generate(saveTo)
        }else
            if (processState == ProcessState.COPY){
                 copy(textFileName, saveTo, loadTo)
            }

        processState = ProcessState.NONE
    }


  private fun copy(targetFileName: String, storage: Storage, loaded: LoadDirection) {
        lunch {
            changeLoad(true)
            val path = chooseFileUri?.let { URIPathHelper.getPath(applicationContext, it) }

          val result = when(loaded){
                LoadDirection.STORAGE ->{
                    path?.let {
                        fileRepository.copy(
                            targetFileName = targetFileName,
                            chooseFilePath = it,
                            storage = storage
                        )
                    }
                }

                LoadDirection.GENERATE ->{
                    fileRepository.copy(targetFileName, storage)
                }
            }

            changeFlow {
                if (result != null) {
                    mResult.emit(result)
                }
            }

            changeLoad(false)
        }

    }


   private fun generate(storage: Storage) {
        lunch {
            changeLoad(true)
            val result = fileRepository.generateLargeFile(storage)
            changeFlow {
                mResult.emit(result)
            }
            changeLoad(false)
        }
    }

    fun setChoseFile(data: Uri?) {
        this.chooseFileUri = data
    }

}