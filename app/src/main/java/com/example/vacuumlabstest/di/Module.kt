package com.example.vacuumlabstest.di

import com.example.vacuumlabstest.data.datastore.FileCopyRepository
import com.example.vacuumlabstest.data.repository.FileCopyRepositoryImpl
import com.example.vacuumlabstest.viewmodel.CopyFileViewModel
import org.koin.dsl.module

val repositoryModule = module {

    single<FileCopyRepository> { FileCopyRepositoryImpl(get()) }
}

val viewModule = module {

    single { CopyFileViewModel(get(), get()) }
}