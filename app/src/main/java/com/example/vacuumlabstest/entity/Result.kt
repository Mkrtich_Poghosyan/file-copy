package com.example.vacuumlabstest.entity

sealed class Result<T>{
    data class Success<T>(val t : T) : Result<T>()
    data class Error<E>(val copyException: CopyException<E>) : Result<E>()
}
