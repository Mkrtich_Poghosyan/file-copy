package com.example.vacuumlabstest.entity

import java.io.IOException

data class CopyException<T>(val exception: IOException?, val t : T, var errorCode : Int = 404)
