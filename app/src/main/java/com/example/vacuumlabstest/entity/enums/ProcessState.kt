package com.example.vacuumlabstest.entity.enums

enum class ProcessState {
    NONE,
    COPY,
    GENERATE;
}