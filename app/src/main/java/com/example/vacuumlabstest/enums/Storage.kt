package com.example.vacuumlabstest.enums

enum class Storage {

    INTERNAL,
    EXTERNAL;
}