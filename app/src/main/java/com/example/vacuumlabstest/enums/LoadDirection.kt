package com.example.vacuumlabstest.enums

enum class LoadDirection {

    STORAGE,
    GENERATE;
}