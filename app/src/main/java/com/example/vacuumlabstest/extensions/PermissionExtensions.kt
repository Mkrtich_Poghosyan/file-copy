package com.example.vacuumlabstest.extensions

import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build

fun Activity.checkPermissionGranted(permission : String, requestCode: Int) : Boolean{
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(permission), requestCode)
            false
        } else {
            true
        }
    } else {
        true
    }
}
