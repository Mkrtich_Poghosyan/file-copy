package com.example.vacuumlabstest.extensions

import android.content.Context
import android.os.Environment
import com.example.vacuumlabstest.enums.Storage
import java.io.*


private val CHARSET = Charsets.UTF_8

fun File.save(text : String) {

    val builder = StringBuilder()
    builder.appendLine( "$text\n")
    appendText(builder.toString(), CHARSET)
}

fun Context.generate(fileName : String, storage : Storage) : File{
    return when (storage) {
        Storage.INTERNAL -> File(filesDir, "$fileName.txt")
        Storage.EXTERNAL -> {
            File(Environment.getExternalStorageDirectory(), "$fileName.txt")
        }
    }
}



