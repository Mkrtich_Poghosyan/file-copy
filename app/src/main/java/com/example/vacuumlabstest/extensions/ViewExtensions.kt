package com.example.vacuumlabstest.extensions

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import com.example.vacuumlabstest.R
import com.example.vacuumlabstest.entity.enums.ProcessState
import com.example.vacuumlabstest.utils.FILE_ALREADY_EXISTS
import com.google.android.material.snackbar.Snackbar

fun View.showOrHide(isShow: Boolean) {
    visibility = if (isShow) View.VISIBLE else View.GONE
}

fun View.showSnackBar(state: ProcessState, isSuccess : Boolean, errorCode : Int = -1) {

    val msg = if (isSuccess){
        when(state){
            ProcessState.COPY -> context.getString(R.string.copy_success)
            ProcessState.GENERATE -> context.getString(R.string.generate_success)
            else -> context.getString(R.string.something_went_wrong)
        }
    }else{
        when(state){
            ProcessState.COPY -> {
                if (errorCode == FILE_ALREADY_EXISTS){
                    context.getString(R.string.copy_fail_name_exist)
                }else{
                    context.getString(R.string.copy_fail)
                }
            }
            ProcessState.GENERATE -> {
                if (errorCode == FILE_ALREADY_EXISTS){
                    context.getString(R.string.generate_fail_name_exist)
                }else{
                    context.getString(R.string.generate_fail)
                }
            }
            else -> context.getString(R.string.something_went_wrong)
        }
    }

    Snackbar.make(this, msg, Snackbar.LENGTH_LONG).show()
}

fun AppCompatEditText.textChange(isNotEmpty : (Boolean, String) -> Unit){
    addTextChangedListener(object : TextWatcher{
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            isNotEmpty.invoke(start >= 0, s.toString())
        }

        override fun afterTextChanged(s: Editable?) {

        }

    })
}