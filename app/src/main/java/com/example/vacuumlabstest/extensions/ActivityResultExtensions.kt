package com.example.vacuumlabstest.extensions

import android.content.Intent
import androidx.activity.ComponentActivity
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult


fun ComponentActivity.startActivityForResultEx(result : (ActivityResult) -> Unit) : ActivityResultLauncher<Intent> {
   val launcher = registerForActivityResult(
        StartActivityForResult()) {
        result.invoke(it)
    }
    return launcher
}